


const socketController = (socket) => {

	console.log('Client connected', socket.id)
	socket.on('disconnect', () => {
	});

	socket.on('enviar-mensaje', (payload, callback) => {
		const id = 7681;
		callback({id, fecha: new Date().getTime()});
		socket.broadcast.emit('enviar-mensaje', payload);
	})

}



module.exports = {
	socketController,
}
